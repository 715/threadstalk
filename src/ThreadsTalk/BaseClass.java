package ThreadsTalk;

/**
 * Created by khada on 12/1/16.
 * Hello, world!
 */
abstract class BaseClass implements Runnable {
    private static long time = System.currentTimeMillis();
    int id;

    void msg() {
        msg("No message");
    }

    void msg(String m) {
        System.out.println("[" + (System.currentTimeMillis() - time + "] " + Thread.currentThread().getName() + ": " + m));
    }
}
