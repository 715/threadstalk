package ThreadsTalk;

/**
 * Created by khada on 12/1/16.
 * Hello, world!
 */
public class FirstClass extends BaseClass {
    private Ping ping;

    FirstClass(int id, Ping ping) {
        this.id = id;
        this.ping = ping;
    }

    @Override
    public void run() {
        Thread.currentThread().setName("First Class " + id);
        msg("constructed");
        for (int i = 0; i < 10; i++) {
            msg(Integer.toString(i));
        }
        ping.pingFirst();
        for (int i = 0; i < 10; i++) {
            msg(Integer.toString(i));
        }
        ping.pingThird();
        ping.pingThirdB();
    }
}
