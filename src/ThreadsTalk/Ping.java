package ThreadsTalk;

/**
 * Created by khada on 12/9/16.
 * Hello, world!
 */
class Ping extends BaseClass {
    private boolean isFirstMethodDone = false;
    private boolean isSecondMethodDone = false;
    private boolean isThirdMethodDone = false;

    synchronized void pingFirst() {
        msg("This message should appear first.");
        if (!isFirstMethodDone) {
            isFirstMethodDone = true;
            notifyAll();
        }
    }
    synchronized void pingSecond() {
        while (!isFirstMethodDone) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        msg("This message should appear after the first message. ");
        if (!isSecondMethodDone) {
            isSecondMethodDone = true;
            notifyAll();
        }
    }

    synchronized void pingThird() {
        while (!isSecondMethodDone) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        msg("This message should appear after the second message. ");
        if (!isThirdMethodDone) {
            isThirdMethodDone = true;
            notifyAll();
        }
    }

    synchronized void pingThirdB() {
        while (!isSecondMethodDone) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        msg("This message should appear after the second message. B ");
    }

    synchronized void pingFourth() {
        while (!isThirdMethodDone) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        msg("This message should appear after the third message.");
    }
    @Override
    public void run() {
    }
}
