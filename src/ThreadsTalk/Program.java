package ThreadsTalk;

/**
 * Created by khada on 12/1/16.
 * Hello, world!
 */
public class Program {
    public static void main(String args[]) throws InterruptedException {
        Ping ping = new Ping();
        final FirstClass firstClass = new FirstClass(0, ping);
        final SecondClass secondClass = new SecondClass(0, ping);
        Thread fThread = new Thread(firstClass);
        Thread sThread = new Thread(secondClass);
        fThread.start();
        sThread.start();
    }
}
