package ThreadsTalk;

/**
 * Created by khada on 12/1/16.
 * Hello, world!
 */
public class SecondClass extends BaseClass {
    private Ping ping;

    SecondClass(int id, Ping ping) {
        this.id = id;
        this.ping = ping;
    }

    @Override
    public void run() {
        Thread.currentThread().setName("Second Class " + id);
        msg("constructed");
        for (int i = 0; i < 10; i++) {
            msg(Integer.toString(i));
        }
        ping.pingSecond();
        ping.pingFourth();
    }
}
